$(function() {
  // Chrome Smooth Scroll
  try {
    $.browserSelector();
    if ($('html').hasClass('chrome')) {
      $.smoothScroll();
    }
  } catch (err) {}

  $('img, a').on('dragstart', function(event) {
    event.preventDefault();
  });
});

// Form init
$(document).ready(function() {
  $('#feedback-form').submit(function() {
      return $.ajax({
          type: 'POST',
          url: 'mail.php',
          data: $(this).serialize()
      }).done(function() {
          swal('Спасибо!', 'Ваш персональный менеджер свяжется с Вами в течении 30 минут.', 'success'), setTimeout(function() {
              var e = $.magnificPopup.instance;
              e.close(), $('form').trigger('reset');
          }, 1e3);
      }), !1;
  });
});

$(document).ready(function() {
  // Preloader init
  $('.preloader').fadeOut();

  // // MagnificPopup init
  // $('.popup-feedback').magnificPopup();
  //
  // // MaskedInput init
  // $('#phone').mask('+38(999) 999-99-99');
});
