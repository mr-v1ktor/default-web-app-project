# The humans responsible & technology colophon

# TEAM

    Viktor Varyanitsa -- web-developer -- https://vk.com/vikt0r_89

# TECHNOLOGY COLOPHON

    CSS3, HTML5, jQuery, Modernizr, Normalize.css, Gulp, Bower, Git, Sass, Jade, Bourbon, PostCSS
