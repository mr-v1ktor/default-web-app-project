var postcss         = require('gulp-postcss');
var lost            = require('lost');
var assets          = require('postcss-assets');
var easysprite      = require('postcss-easysprites');
var pixrem          = require('pixrem');
var pxtoem          = require('postcss-px-to-em');
var gulp            = require('gulp');
var sass            = require('gulp-sass');
var autoprefixer    = require('autoprefixer');
var sourcemaps      = require('gulp-sourcemaps');
var csscomb         = require('gulp-csscomb');
var concat          = require('gulp-concat');
var uglify          = require('gulp-uglify');
var jade            = require('gulp-jade');
var plumber         = require('gulp-plumber');
var notify          = require('gulp-notify');
var mainBowerFiles  = require('main-bower-files');
var filter          = require('gulp-filter');
var order           = require('gulp-order');
var bourbon         = require('node-bourbon');
var browserSync     = require('browser-sync');

gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'app'
    },
    notify: false,
    logPrefix: "2Kings server"
  });
});

gulp.task('sass', function() {
  var processors = [
    pxtoem(),
    assets({
      basePath: 'app/',
      loadPaths: ['img/']
    }),
    easysprite({
      imagePath:'img/icons',
      spritePath: 'app/img/icons/sprites'
    }),
    lost(),
    autoprefixer({ browsers: ['last 10 versions'] }),
    pixrem()
  ];
  gulp.src('app/sass/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({
    includePaths: bourbon.includePaths
  }).on('error', sass.logError))
  .pipe(plumber({
    errorHandler: notify.onError(function(err) {
      return {
        title: 'Sass',
        message: err.message
      };
    })
  }))
  .pipe(postcss(processors))
  .pipe(csscomb())
  .pipe(sourcemaps.write('/maps'))
  .pipe(gulp.dest('app/css'))
  .pipe(browserSync.reload({stream: true}));
});

gulp.task('jade', function() {
  return gulp.src('app/jade/*.jade')
  .pipe(plumber({
    errorHandler: notify.onError(function(err) {
      return {
        title: 'Jade',
        message: err.message
      };
    })
  }))
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest('app'));
});

gulp.task('vendorJS', function () {
  var vendors = mainBowerFiles();
  return gulp.src(vendors)
    .pipe(plumber({
        errorHandler: notify.onError(function(err) {
          return {
            title: 'vendorJS',
            message: err.message
          };
        })
      }))
     .pipe(filter('**/*.js'))
     .pipe(order(vendors))
     .pipe(concat('libs.js'))
     .pipe(gulp.dest('app/js'));
});

gulp.task('vendorCSS', function () {
  var vendors = mainBowerFiles();
  return gulp.src(vendors)
    .pipe(plumber({
        errorHandler: notify.onError(function(err) {
          return {
            title: 'vendorCSS',
            message: err.message
          };
        })
      }))
     .pipe(filter('**/*.css'))
     .pipe(order(vendors))
     .pipe(concat('libs.css'))
     .pipe(gulp.dest('app/css'));
});

gulp.task('watch',['browserSync','jade','vendorJS','vendorCSS'],function() {
    gulp.watch('bower.json');
    gulp.watch('app/sass/**/*.scss', ['sass']);
    gulp.watch('app/jade/**/*.jade', ['jade']);
    gulp.watch('app/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['watch']);
